#!/bin/sh
# vim: ts=4 sts=4 sw=4 et

set -e

install_checking_tools(){
    go get github.com/golang/lint/golint    \
           github.com/fzipp/gocyclo         \
           github.com/client9/misspell/...  \
           github.com/gordonklaus/ineffassign
}

install_checking_scripts() {
    DOT_GIT="$(dirname $0)/../.git"

    for fname in check_email.sh             \
                 check_gpgsign.sh           \
                 check_golang.sh            \
                 init_colors.inc.sh         \
                 pre-commit
    do
        ln -sfv "../../git_hooks/$fname" "$DOT_GIT/hooks/$fname"
    done
}

install_checking_tools
install_checking_scripts
