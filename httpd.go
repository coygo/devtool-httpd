// Copyright 2018 Coy. All rights reserved.
// Use of this source code is governed by a GPLv3
// license that can be found in the LICENSE file.

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/yookoala/realpath"

	"gitlab.com/coygo/log"
)

var (
	addr      string
	basePath  string
	transFile bool
)

func main() {
	log.Infof("Serving for %q on %q", basePath, addr)

	http.HandleFunc("/", myHandler)

	s := &http.Server{
		Addr:           addr,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(s.ListenAndServe())
}

func init() {
	flag.StringVar(&addr, "addr", "localhost:8080", "serve on address")
	flag.StringVar(&addr, "a", "localhost:8080", "serve on address")
	flag.BoolVar(&transFile, "trans-file", false, "")
	flag.BoolVar(&transFile, "t", false, "")
	var isHelp bool
	flag.BoolVar(&isHelp, "help", false, "print this message")
	flag.BoolVar(&isHelp, "h", false, "print this message")
	var isDebug bool
	flag.BoolVar(&isDebug, "debug", false, "print debug information")
	flag.BoolVar(&isDebug, "d", false, "print debug information")
	flag.Parse()

	if isHelp {
		flag.Usage()
		os.Exit(0)
	}

	if isDebug {
		log.SetFlags(log.LstdFlags | log.Lshortfile | log.Ltrace)
	} else {
		log.SetFlags(log.LstdFlags)
	}

	if len(flag.Args()) != 1 {
		flag.Usage()
		os.Exit(1)
	}
	log.Debugf("flag.Args()=%+v", flag.Args())

	var err error
	basePath, err = realpath.Realpath(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}

	if _, err := os.Stat(basePath); os.IsNotExist(err) {
		fmt.Printf("Path not found: %q\n", basePath)
		os.Exit(2)
	}
}

func myHandler(w http.ResponseWriter, r *http.Request) {
	log.Infof("%s %s %s", getRemoteAddr(r), r.Method, r.URL)

	f, err := os.Open(path.Join(basePath, r.RequestURI))

	if os.IsNotExist(err) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Write(requestToJSON(r, "", "  "))
		return
	}

	if transFile {
		io.Copy(w, f) // NOTE: ignored error
		f.Close()
		return
	}

	if err != nil {
		log.Errorln(err) // Open error
	}
}

func getRemoteAddr(r *http.Request) string {
	if r.Header.Get("X-Real-IP") != "" {
		return r.Header.Get("X-Real-IP")
	}
	if r.Header.Get("X-Forwarded-For") != "" {
		return r.Header.Get("X-Forwarded-For")
	}

	re := regexp.MustCompile(`^\[?([^]]+)\]?:\d+$`)
	ipAddr := net.ParseIP(re.FindAllStringSubmatch(r.RemoteAddr, -1)[0][1])
	return ipAddr.String()
}

func requestToJSON(r *http.Request, prefix, indent string) []byte {
	request := struct {
		RemoteAddr    string
		Method        string
		Host          string
		RequestURI    string
		URL           string
		Proto         string
		Header        string
		UserAgent     string
		Body          []byte
		ContentLength int64
		Encoding      string
		Closed        bool
		Form          string
		PostForm      string
		MultipartForm string
		Trailer       http.Header
	}{
		RemoteAddr:    r.RemoteAddr,
		Method:        r.Method,
		Host:          r.Host,
		RequestURI:    r.RequestURI,
		URL:           r.URL.String(),
		Proto:         r.Proto,
		UserAgent:     r.UserAgent(),
		ContentLength: r.ContentLength,
		Encoding:      strings.Join(r.TransferEncoding, ", "),
		Closed:        r.Close,
		Trailer:       r.Trailer,
	}

	// Header
	if r.Header != nil {
		var header []string
		for k, v := range r.Header {
			header = append(header, fmt.Sprintf("%s=%+v", k, v))
		}
		request.Header = strings.Join(header, ", ")
	}

	// Forms
	r.ParseMultipartForm(0) // 0 byte + 10MB
	// Forms: Form
	if r.Form != nil {
		var form []string
		for k, v := range r.Form {
			form = append(form, fmt.Sprintf("%s=%+v", k, v))
		}
		request.Form = strings.Join(form, ", ")
	}
	// Forms: PostForm
	if r.PostForm != nil {
		var postForm []string
		for k, v := range r.PostForm {
			postForm = append(postForm, fmt.Sprintf("%s=%+v", k, v))
		}
		request.PostForm = strings.Join(postForm, ", ")
	}
	// Forms: MultipartForm
	if r.MultipartForm != nil {
		var multipartForm []string
		for k, v := range r.MultipartForm.Value {
			multipartForm = append(multipartForm, fmt.Sprintf("%s=%+v", k, v))
		}
		request.MultipartForm = strings.Join(multipartForm, ", ")
	}

	// Body
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		log.Error(err)
		return nil
	}
	request.Body = body

	j, err := json.MarshalIndent(request, prefix, indent)
	if err != nil {
		log.Error(err)
		return nil
	}

	return j
}
